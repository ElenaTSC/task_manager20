package ru.tsk.ilina.tm.model;

public abstract class AbstractOwnerEntity extends AbstractEntity {

    private String userId;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

}
