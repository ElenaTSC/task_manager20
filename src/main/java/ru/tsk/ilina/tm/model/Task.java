package ru.tsk.ilina.tm.model;

import ru.tsk.ilina.tm.api.entity.IWBS;
import ru.tsk.ilina.tm.enumerated.Status;

import java.util.Date;
import java.util.UUID;

public class Task extends AbstractBusinessEntity implements IWBS {

    private String projectId = null;

    public Task() {

    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

}
