package ru.tsk.ilina.tm.repository;

import ru.tsk.ilina.tm.api.repository.IAbstractBusinessRepository;
import ru.tsk.ilina.tm.api.repository.IAbstractOwnerRepository;
import ru.tsk.ilina.tm.api.repository.IAbstractRepository;
import ru.tsk.ilina.tm.model.AbstractBusinessEntity;
import ru.tsk.ilina.tm.model.AbstractOwnerEntity;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractOwnerRepository<E extends AbstractOwnerEntity> extends AbstractRepository<E> implements IAbstractOwnerRepository<E> {

    @Override
    public void remove(final String userId, final E entity) {
        if (!userId.equals(entity.getUserId())) return;
        entities.remove(entity);
    }

    @Override
    public void clear(final String userId) {
        final List<E> entityList = new ArrayList<>();
        for (final E entity : entities) {
            if (userId.equals(entity.getUserId())) entityList.add(entity);
        }
        entityList.clear();
    }

    @Override
    public Integer getSize(String userId) {
        final List<E> result = new ArrayList<>();
        for (final E entity : entities)
            return result.size();
        return null;
    }

    @Override
    public void add(final String userId, final E entity) {
        entity.setUserId(userId);
        entities.add(entity);
    }

    @Override
    public List<E> findAll(final String userId) {
        final List<E> result = new ArrayList<>();
        for (final E entity : entities) {
            if (userId.equals(entity.getUserId())) result.add(entity);
        }
        return result;
    }

    @Override
    public List<E> findAll(final String userId, final Comparator<E> comparator) {
        final List<E> result = new ArrayList<>();
        for (final E entity : entities) {
            if (userId.equals(entity.getUserId())) result.add(entity);
        }
        result.sort(comparator);
        return result;
    }

    @Override
    public E findByIndex(final String userId, final Integer index) {
        final List<E> entity = findAll(userId);
        return entity.get(index);
    }

    @Override
    public boolean existsById(String userId, String id) {
        return findByID(userId, id) != null;
    }
    @Override
    public E removeByIndex(final String userId, final Integer index) {
        final E entity = findByIndex(userId, index);
        if (entity == null) return null;
        entities.remove(entity);
        return entity;
    }

    @Override
    public E findByID(final String userId, final String id) {
        for (E entity : entities) {
            if (!userId.equals(entity.getUserId())) continue;
            if (id.equals(entity.getId())) return entity;
        }
        return null;
    }

    @Override
    public E removeByID(final String userId, final String id) {
        final E entity = findByID(userId, id);
        if (entity == null) return null;
        entities.remove(entity);
        return entity;
    }

}
