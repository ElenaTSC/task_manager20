package ru.tsk.ilina.tm.repository;

import ru.tsk.ilina.tm.api.repository.IUserRepository;
import ru.tsk.ilina.tm.model.User;

import java.util.ArrayList;
import java.util.List;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    public User findByEmail(final String email) {
        for (final User user : entities) {
            if (email.equals(user.getEmail())) return user;
        }
        return null;
    }

    @Override
    public User removeByLogin(final String login) {
        final User user = findByLogin(login);
        if (user == null) return null;
        return remove(user);
    }

    @Override
    public User findByLogin(final String login) {
        for (final User user : entities) {
            if (login.equals(user.getLogin())) return user;
        }
        return null;
    }

}
