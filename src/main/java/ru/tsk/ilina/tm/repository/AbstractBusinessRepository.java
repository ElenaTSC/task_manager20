package ru.tsk.ilina.tm.repository;

import ru.tsk.ilina.tm.api.repository.IAbstractBusinessRepository;
import ru.tsk.ilina.tm.enumerated.Status;
import ru.tsk.ilina.tm.model.*;

public abstract class AbstractBusinessRepository<E extends AbstractBusinessEntity> extends AbstractOwnerRepository<E> implements IAbstractBusinessRepository<E> {

    @Override
    public E removeByName(final String userId, final String name) {
        final E entity = findByName(userId, name);
        if (entity == null) return null;
        entities.remove(entity);
        return entity;
    }

    @Override
    public E findByName(final String userId, final String name) {
        for (E entity : entities) {
            if (!userId.equals(entity.getUserId())) continue;
            if (name.equals(entity.getName())) return entity;
        }
        return null;
    }

    @Override
    public E startByID(final String userId, String id) {
        final E entity = findByID(userId, id);
        if (entity == null) return null;
        entity.setStatus(Status.IN_PROGRESS);
        return entity;
    }

    @Override
    public E startByIndex(final String userId, Integer index) {
        final E entity = findByIndex(userId, index);
        if (entity == null) return null;
        entity.setStatus(Status.IN_PROGRESS);
        return entity;
    }

    @Override
    public E startByName(final String userId, String name) {
        final E entity = findByName(userId, name);
        if (entity == null) return null;
        entity.setStatus(Status.IN_PROGRESS);
        return entity;
    }

    @Override
    public E changeStatusByID(final String userId, String id, Status status) {
        final E entity = findByID(userId, id);
        if (entity == null) return null;
        entity.setStatus(status);
        return entity;
    }

    @Override
    public E changeStatusByIndex(final String userId, Integer index, Status status) {
        final E entity = findByIndex(userId, index);
        if (entity == null) return null;
        entity.setStatus(status);
        return entity;
    }

    @Override
    public E changeStatusByName(final String userId, String name, Status status) {
        final E entity = findByName(userId, name);
        if (entity == null) return null;
        entity.setStatus(status);
        return entity;
    }

    @Override
    public E finishByID(final String userId, String id) {
        final E entity = findByID(userId, id);
        if (entity == null) return null;
        entity.setStatus(Status.COMPLETED);
        return entity;
    }

    @Override
    public E finishByIndex(final String userId, Integer index) {
        final E entity = findByIndex(userId, index);
        if (entity == null) return null;
        entity.setStatus(Status.COMPLETED);
        return entity;
    }

    @Override
    public E finishByName(final String userId, String name) {
        final E entity = findByName(userId, name);
        if (entity == null) return null;
        entity.setStatus(Status.COMPLETED);
        return entity;
    }

}
