package ru.tsk.ilina.tm.command.auth;

import ru.tsk.ilina.tm.api.service.IUserService;
import ru.tsk.ilina.tm.command.AbstractAuthUserCommand;
import ru.tsk.ilina.tm.util.TerminalUtil;

public class AuthUpdateProfileCommand extends AbstractAuthUserCommand {

    @Override
    public String name() {
        return "update";
    }

    @Override
    public String description() {
        return "Update information about user";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[UPDATE PROFILE]");
        System.out.println("ENTER FIRST NAME: ");
        final String firstName = TerminalUtil.nextLine();
        System.out.println("ENTER LAST NAME: ");
        final String lastName = TerminalUtil.nextLine();
        System.out.println("ENTER MIDDLE NAME: ");
        final String middleName = TerminalUtil.nextLine();
        serviceLocator.getAuthService().updateUser(userId, firstName, lastName, middleName);
        System.out.println("[OK]");
    }

}
