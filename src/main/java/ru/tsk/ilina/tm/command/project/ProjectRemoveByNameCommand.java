package ru.tsk.ilina.tm.command.project;

import ru.tsk.ilina.tm.command.AbstractProjectCommand;
import ru.tsk.ilina.tm.exception.entity.ProjectNotFoundException;
import ru.tsk.ilina.tm.model.Project;
import ru.tsk.ilina.tm.util.TerminalUtil;

public final class ProjectRemoveByNameCommand extends AbstractProjectCommand {

    @Override
    public String name() {
        return "project-remove-by-name";
    }

    @Override
    public String description() {
        return "Remove project by name";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[ENTER NAME]");
        final String name = TerminalUtil.nextLine();
        final Project project = serviceLocator.getProjectService().findByName(userId, name);
        if (project == null) throw new ProjectNotFoundException();
        serviceLocator.getProjectService().removeByName(userId, name);
    }

}
