package ru.tsk.ilina.tm.service;

import ru.tsk.ilina.tm.api.repository.IAbstractBusinessRepository;
import ru.tsk.ilina.tm.api.service.IAbstractBusinessService;
import ru.tsk.ilina.tm.enumerated.Status;
import ru.tsk.ilina.tm.exception.empty.*;
import ru.tsk.ilina.tm.exception.entity.ProjectNotFoundException;
import ru.tsk.ilina.tm.model.AbstractBusinessEntity;
import ru.tsk.ilina.tm.model.Task;

public abstract class AbstractBusinessService<E extends AbstractBusinessEntity,R extends IAbstractBusinessRepository<E>> extends AbstractOwnerService<E,R> implements IAbstractBusinessService<E> {

    protected AbstractBusinessService(R repository) {
        super(repository);
    }

    @Override
    public E removeByName(final String userId, String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return repository.removeByName(userId, name);
    }

    @Override
    public E findByName(final String userId, final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return repository.findByName(userId, name);
    }

    @Override
    public E updateById(final String userId, final String id, final String name, final String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final E entity = repository.findByID(userId, id);
        entity.setName(name);
        entity.setDescription(description);
        return entity;
    }

    @Override
    public E changeStatusByID(final String userId, final String id, final Status status) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (status == null) throw new EmptyStatusException();
        return repository.changeStatusByID(userId, id, status);
    }

    @Override
    public E changeStatusByName(final String userId, final String name, final Status status) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (status == null) throw new EmptyStatusException();
        return repository.changeStatusByName(userId, name, status);
    }

    @Override
    public E finishByName(final String userId, String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return repository.finishByName(userId, name);
    }
    public E startByName(final String userId, String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return repository.startByName(userId, name);
    }

    public E finishByID(final String userId, String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return repository.finishByID(userId, id);
    }

    public E startByID(final String userId, String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return repository.startByID(userId, id);
    }
    @Override
    public E removeByIndex(final String userId, Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        if (repository.getSize(userId) < index - 1) return null;
        return repository.removeByIndex(userId, index);
    }

    @Override
    public E findByIndex(final String userId, final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        if (repository.getSize(userId) < index - 1) return null;
        return repository.findByIndex(userId, index);
    }

    @Override
    public E updateByIndex(final String userId, final Integer index, final String name, final String description) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (index == null || index < 0) throw new EmptyIdException();
        if (repository.getSize(userId) < index - 1) throw new ProjectNotFoundException();
        final E entity = repository.findByIndex(userId, index);
        entity.setName(name);
        entity.setDescription(description);
        return entity;
    }

    @Override
    public E startByIndex(final String userId, final Integer index) {
        final E entity = findByIndex(userId, index);
        if (entity == null) return null;
        entity.setStatus(Status.IN_PROGRESS);
        return entity;
    }

    @Override
    public E finishByIndex(final String userId, final Integer index) {
        final E entity = findByIndex(userId, index);
        if (entity == null) return null;
        entity.setStatus(Status.COMPLETED);
        return entity;
    }

    @Override
    public E changeStatusByIndex(final String userId, final Integer index, final Status status) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        if (repository.getSize(userId) < index - 1) return null;
        if (status == null) throw new EmptyStatusException();
        return repository.changeStatusByIndex(userId, index, status);
    }

}
