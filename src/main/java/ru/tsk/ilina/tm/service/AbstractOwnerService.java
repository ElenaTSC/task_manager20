package ru.tsk.ilina.tm.service;

import ru.tsk.ilina.tm.api.repository.IAbstractOwnerRepository;
import ru.tsk.ilina.tm.api.repository.IAbstractRepository;
import ru.tsk.ilina.tm.api.service.IAbstractOwnerService;
import ru.tsk.ilina.tm.exception.empty.EmptyIdException;
import ru.tsk.ilina.tm.exception.empty.EmptyUserIdException;
import ru.tsk.ilina.tm.model.AbstractOwnerEntity;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractOwnerService <E extends AbstractOwnerEntity,R extends IAbstractOwnerRepository<E>> extends AbstractService<E,R> implements IAbstractOwnerService<E> {

    protected AbstractOwnerService(R repository) {
        super(repository);
    }

    @Override
    public void clear(final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        repository.clear(userId);
    }

    @Override
    public List<E> findAll(final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        return repository.findAll(userId);
    }

    @Override
    public List<E> findAll(final String userId, Comparator<E> comparator) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (comparator == null) return Collections.emptyList();
        return repository.findAll(userId, comparator);
    }

    @Override
    public E removeByID(final String userId, String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return repository.removeByID(userId, id);
    }

    @Override
    public E findByID(final String userId, final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return repository.findByID(userId, id);
    }

    @Override
    public void add(final String userId, final E entity) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (entity == null) return;
        repository.add(userId, entity);
    }

    @Override
    public void remove(String userId, E entity) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (entity == null) return;
        repository.remove(userId, entity);
    }

}
