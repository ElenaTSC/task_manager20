package ru.tsk.ilina.tm.exception.user;

import ru.tsk.ilina.tm.exception.AbstractException;

public class LoginExistsException extends AbstractException {

    public LoginExistsException() {
        super("Error! Login already exists");
    }

    public LoginExistsException(String message) {
        super("Error! " + message + " login already exists");
    }

}
